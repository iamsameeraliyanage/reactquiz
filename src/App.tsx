import React, { useState } from "react";
import { Dificulty, fetchQuizQuestions, QuestionState } from "./API";
import QuestionCard from "./componenets/QuestionCard";
const TOTLA_QUESTIONS = 10;

export type AnswerObject = {
  question: string;
  answer: string;
  correct: boolean;
  correctAnswer: string;
};

function App() {
  const [loading, setLoading] = useState(false);
  const [questions, setQuestions] = useState<QuestionState[]>([]);
  const [questionNumber, setQuestionNumber] = useState(0);
  const [userAnswers, setUserAnswers] = useState<AnswerObject[]>([]);
  const [score, setScore] = useState(0);
  const [gameOver, setGameOver] = useState(true);

  const startQuiz = async () => {
    setLoading(true);
    setGameOver(false);
    const newQuestions = await fetchQuizQuestions(
      TOTLA_QUESTIONS,
      Dificulty.EASY
    );
    setQuestions(newQuestions);
    setScore(0);
    setUserAnswers([]);
    setQuestionNumber(0);
    setLoading(false);
  };
  const checkAnswer = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (!gameOver) {
      const answer = e.currentTarget.value;
      const correct = questions[questionNumber].correct_answer === answer;
      if (correct) {
        setScore((prev) => prev + 1);
      }
      const answerObject: AnswerObject = {
        question: questions[questionNumber].question,
        answer,
        correct,
        correctAnswer: questions[questionNumber].correct_answer,
      };

      setUserAnswers((prev) => {
        return [...prev, answerObject];
      });
    }
  };

  const goToNextQuestion = () => {
    const nextQuestionNo = questionNumber + 1;
    if (nextQuestionNo === TOTLA_QUESTIONS) {
      setGameOver(true);
    } else {
      setQuestionNumber(nextQuestionNo);
    }
  };

  return (
    <div className="App">
      <h1>React Quiz</h1>

      {gameOver || userAnswers.length === TOTLA_QUESTIONS ? (
        <button onClick={startQuiz}>Start</button>
      ) : null}
      {!gameOver ? <p>{score}</p> : null}
      {loading && <p>loading questions</p>}
      {!loading && !gameOver && (
        <QuestionCard
          questionNo={questionNumber}
          totalQuestions={TOTLA_QUESTIONS}
          question={questions[questionNumber].question}
          answers={questions[questionNumber].answers}
          userAnswer={userAnswers ? userAnswers[questionNumber] : undefined}
          callBack={checkAnswer}
        />
      )}
      {!gameOver &&
        !loading &&
        userAnswers.length === questionNumber + 1 &&
        questionNumber !== TOTLA_QUESTIONS - 1 && (
          <button onClick={goToNextQuestion}>Next</button>
        )}
    </div>
  );
}

export default App;
