import { shuffleArray } from "./utils";
export enum Dificulty {
  EASY = "easy",
  MEDIUM = "medium",
  HARD = "hard",
}
export type Question = {
  category: string;
  correct_answer: string;
  difficulty: string;
  incorrect_answers: string[];
  question: string;
  type: string;
};

export type QuestionState = Question & {
  answers: string[];
};
export const fetchQuizQuestions = async (
  amount: number,
  dificulty: Dificulty
) => {
  const endpont = `https://opentdb.com/api.php?amount=${amount}&dificulty=${dificulty}&type=multiple`;
  const data = await (await fetch(endpont)).json();
  console.log(data);
  return data.results.map((question: Question) => {
    return {
      ...question,
      answers: shuffleArray([
        ...question.incorrect_answers,
        question.correct_answer,
      ]),
    };
  });
};
