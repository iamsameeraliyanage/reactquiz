import React from "react";
import { AnswerObject } from "../App";

interface QuestionCardProps {
  question: string;
  answers: string[];
  callBack: (e: React.MouseEvent<HTMLButtonElement>) => void;
  userAnswer: AnswerObject | undefined;
  questionNo: number;
  totalQuestions: number;
}
const QuestionCard = ({
  question,
  answers,
  callBack,
  questionNo,
  totalQuestions,
  userAnswer,
}: QuestionCardProps) => {
  return (
    <div>
      <p>
        Question:{questionNo + 1}/{totalQuestions}
      </p>
      <p>{question}</p>
      <div>
        {answers.map((answer, index) => {
          return (
            <div key={index}>
              <button disabled={!!userAnswer} value={answer} onClick={callBack}>
                <span>{answer}</span>
              </button>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default QuestionCard;
